# A Dictionary is a collection which is unordered
#Changeble and indexed
#No duplicated members

#create dist

person={"FirstName": "Deepak N",
        "lastName": "Narayanasamy",
        'aga': 22}

# print(person)

# use constructor
# person2= dict("FirstName": "Deepak","lastName": "Narayanasamy")

# print(person["FirstName"])

person['phonenumber']='6380839316'

# print(person)
#
# #get only items
#
# print(person.items())
#
# print(person.keys())
# print(person.value

# person.pop('phonenumber')
# print(person)


products =[
        {'brand': 'Lenovo', 'price': 20000},
        {'brand': 'Dell', 'price': 210000},
        {'brand': 'Acer', 'price': 230000},
        {'brand': 'HP', 'price': 240000},
        {'brand': 'MAC', 'price': 250000},

]

print(products[2]['price'])

print(products[2]['brand'])