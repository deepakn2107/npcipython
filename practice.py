# Write a Python program to append a new item to the end of the array.
from array import array

a=array('i',[1,3,5,7,9])
# a[6]=11
print(a)
a.append(11)
print(a)

# Write a Python program to get the number of occurrences of a specified element in an array
b=array('i', [1, 3, 5, 3, 7, 9, 3])
c=b.count(3)
print("Number of occurrences of the number 3 in the said array:" ,c)


# Write a Python program to insert a new item before the second element in an existing array.

a.insert(2,4)

print(a)


# Write a Python function that takes a list of words and return the longest word and the length of the longest one
# Sample Output:

word="Exercise"

print(len(word))

# Write a Python function to sum all the numbers in a list
sum=0
sample_list=[8,2,3,0,7]
for i in sample_list:
    sum+=i

print(sum)


# 6. Write a Python program to reverse a string. Go to the editor

Sample_string="1234abcd"


print(sample_list.reverse())