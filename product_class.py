class product:
    def __init__(self, brand, price, stocksLeft):
        self.brand = brand
        self.price = price
        self.stocksLeft = stocksLeft

    def stocksDetails(self):
        print(f"Brand Name : {self.brand} Price : {self.price}  Stocks left in store: {self.stocksLeft}")
    def remainingStocks(self):
        self.stocksLeft-=1

redmi = product("Redmi", 15000, 10)

realme = product("Realme", 13000, 10)
vivo = product("Vivo", 13999, 11)
iphone = product("Iphone ", 40000, 10)

brands = input("Enter brand from the list redmi, realme, vivo, iphone: ")
print(brands)
if (brands == "redmi"):
    redmi.remainingStocks()
    redmi.stocksDetails()

elif (brands == "realme"):
    realme.remainingStocks()
    realme.stocksDetails()

elif (brands == "vivo"):
    vivo.remainingStocks()
    vivo.stocksDetails()

elif (brands == "iphone"):
    iphone.remainingStocks()
    iphone.stocksDetails()

else:
    print(f"{brands} not found")
